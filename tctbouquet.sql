-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2022 at 09:43 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tctbouquet`
--

-- --------------------------------------------------------

--
-- Table structure for table `admininfo`
--

CREATE TABLE `admininfo` (
  `AdminID` int(30) NOT NULL,
  `Adminname` varchar(80) NOT NULL,
  `Adminemail` varchar(80) NOT NULL,
  `Adminpassword` varchar(80) NOT NULL,
  `Admindelete` int(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admininfo`
--

INSERT INTO `admininfo` (`AdminID`, `Adminname`, `Adminemail`, `Adminpassword`, `Admindelete`) VALUES
(1201201983, 'Chee Siao Ting', 'Cheeadmin@email.com', '12345678', 0),
(1201202627, 'Tan Hann Wei', 'Tanadmin@email.com', '12345678', 0),
(1201202707, 'Tay Jia Yi', 'Tayadmin@email.com', '12345678', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cartinfo`
--

CREATE TABLE `cartinfo` (
  `CartID` int(30) NOT NULL,
  `Cartquantity` varchar(80) NOT NULL,
  `Orderdel` int(20) NOT NULL DEFAULT 0,
  `OrderID` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orderinfo`
--

CREATE TABLE `orderinfo` (
  `OrderID` int(30) NOT NULL,
  `Orderdate` date NOT NULL,
  `Orderprice` decimal(3,2) NOT NULL,
  `Orderdetail` varchar(80) NOT NULL,
  `Orderimage` varchar(20) NOT NULL,
  `Orderdel` int(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `paymentinfo`
--

CREATE TABLE `paymentinfo` (
  `PayID` int(30) NOT NULL,
  `PayNum` varchar(80) NOT NULL,
  `PayDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `PayType` int(20) NOT NULL,
  `PayTotal` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `productinfo`
--

CREATE TABLE `productinfo` (
  `ProductID` int(30) NOT NULL,
  `Productname` varchar(80) NOT NULL,
  `Productprice` decimal(3,2) NOT NULL,
  `Productdetail` varchar(80) NOT NULL,
  `Productimage` varchar(20) NOT NULL,
  `Productdel` int(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `suadmininfo`
--

CREATE TABLE `suadmininfo` (
  `SUAdminID` int(30) NOT NULL,
  `SUAdminname` varchar(80) NOT NULL,
  `SUAdminemail` varchar(80) NOT NULL,
  `SUAdminpassword` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `suadmininfo`
--

INSERT INTO `suadmininfo` (`SUAdminID`, `SUAdminname`, `SUAdminemail`, `SUAdminpassword`) VALUES
(1201201983, 'CHEE SIAO TING', 'Cheesuadmin@email.com', '123456789'),
(1201202627, 'TAN HANN WEI', 'Tansuadmin@email.com', '123456789'),
(1201202707, 'TAY JIA Yi', 'Taysuadmin@email.com', '123456789');

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `UserID` int(30) NOT NULL,
  `Username` varchar(80) NOT NULL,
  `Useremail` varchar(80) NOT NULL,
  `Userpassword` varchar(80) NOT NULL,
  `Userphoneno` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admininfo`
--
ALTER TABLE `admininfo`
  ADD PRIMARY KEY (`AdminID`);

--
-- Indexes for table `cartinfo`
--
ALTER TABLE `cartinfo`
  ADD PRIMARY KEY (`CartID`);

--
-- Indexes for table `orderinfo`
--
ALTER TABLE `orderinfo`
  ADD PRIMARY KEY (`OrderID`);

--
-- Indexes for table `paymentinfo`
--
ALTER TABLE `paymentinfo`
  ADD PRIMARY KEY (`PayID`);

--
-- Indexes for table `productinfo`
--
ALTER TABLE `productinfo`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `suadmininfo`
--
ALTER TABLE `suadmininfo`
  ADD PRIMARY KEY (`SUAdminID`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admininfo`
--
ALTER TABLE `admininfo`
  MODIFY `AdminID` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1201202708;

--
-- AUTO_INCREMENT for table `cartinfo`
--
ALTER TABLE `cartinfo`
  MODIFY `CartID` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderinfo`
--
ALTER TABLE `orderinfo`
  MODIFY `OrderID` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentinfo`
--
ALTER TABLE `paymentinfo`
  MODIFY `PayID` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productinfo`
--
ALTER TABLE `productinfo`
  MODIFY `ProductID` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suadmininfo`
--
ALTER TABLE `suadmininfo`
  MODIFY `SUAdminID` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1201202708;

--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `UserID` int(30) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
